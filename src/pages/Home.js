import { Container, Row, Col, Button } from "react-bootstrap";
import {Link} from "react-router-dom";

import Introduction from "../components/Introduction";

export default function Home(){
	return (

		<>
		<Container fluid className="my-5">
			<Row>
				<Col>
					<img src = "../banner_crafty.gif"  className="img-fluid" />				
				</Col>
			</Row>

		</Container>

		{/*<Container fluid className="m-auto">
			<Row>
				<Col>
					<Button as = {Link} to ="/products" className = "justify-content-center" variant="danger" type="submit" id="submitBtn">Shop Here</Button>					
				</Col>
			</Row>
		</Container>
		*/}
		
		<Introduction />
		</>
	)
}