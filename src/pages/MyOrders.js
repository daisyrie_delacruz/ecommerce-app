import {useContext, useEffect, useState} from "react";

import { Table } from "react-bootstrap";

import {Navigate} from "react-router-dom";

import UserContext from "../UserContext";

export default function MyOrders() {

	const { user } = useContext(UserContext);

	const { allOrders, setAllOrders } = useState([]);

	const [ orderId, setOrderId ] = useState('');
	const [ totalAmount, setTotalAmount] = useState(0);
	const [ orders, setOrders] = useState([]);


	const fetchOrders = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/yourOrder`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return (
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.products.productId}</td>
						<td>{order.products.productName}</td>
						<td>{order.products.quantity}</td>
						<td>{order.totalAmount}</td>
					</tr>
				)
			}));
		});
	}

	return (

		(user.isAdmin)
		?
		<Navigate to ="/admin"/>
		:

		<>
		<h1 className = "mt-5 mb-3 text-center header">Order History</h1>
		
		<Table striped bordered hover>
			<thead>
				<tr className ="text-center">
				  <th>Order ID</th>
				  <th>Product ID</th>
				  <th>Product Name</th>
				  <th>Quantity</th>
				  <th>Date Purchased</th>
				  <th>Total Amount</th>				  
				</tr>
			</thead>
			<tbody className ="text-center">
				{allOrders}
			</tbody>
		</Table>
		</>
	)
}