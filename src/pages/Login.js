import { useEffect, useContext, useState } from "react";
import { Form, Button, Container } from "react-bootstrap";

import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Login(){

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if (email !== '' && password !== ""){
			setIsActive(true);
		}
		else {
			setIsActive(false);
		}
	}, [email, password]);

	function loginUser(e){
		
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.accessToken);

			if(data.accessToken !== undefined){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login successful!",
					icon: "success",
					text: "Welcome Back!"
				});
			}
			else{
				Swal.fire({
					title: "Login failed!",
					icon: "error",
					text: "Please check your email/password and try again."
				});
			}
		})

		setEmail('');
		setPassword('')
	}

	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	}


	return(

		(user.id !== null)
		?
			<Navigate to ="/products"/>
		:

		<>
		<Container className="d-flex my-5" id="container-login" >

		<Form onSubmit = {e => loginUser(e)} className=" col-12 col-md-6 ">

		<h1 className="my-5 text-center header">Login</h1>

			<Form.Group className="m-3" controlId="emailAddress" >
		    	<Form.Label>Email Address</Form.Label>
		    	<Form.Control 
		    		type="email" 
		    		placeholder="Enter email"
		    		value = {email}
		    		onChange={e => setEmail(e.target.value)}		    		
		    		required
		    	/>
		  	</Form.Group>

		  	<Form.Group className="m-3" controlId="password">
		    	<Form.Label>Password</Form.Label>
		    	<Form.Control 
		    	type="password" 
		    	placeholder="Enter password"
		    	value = {password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    	/>
		  	</Form.Group>
		  	<p className = "text-center">No account yet? Sign up <a href="/register" className="text-dark"> here.</a></p>

		  	{
				isActive
				?
				<Container className = "text-center mb-5">
					<Button className = "m-auto" variant="danger" type="submit" id="submitBtn">
					Login
					</Button>
				</Container>
				:
				<Container className = "text-center mb-5">
					<Button className = "m-auto"variant="danger" type="submit" id="submitBtn" disabled>
					  Login
					</Button>
				</Container>
			}
		</Form>
		<img src = "../craftyclub_logo.png" width="300" height="300" className="m-auto prod-card"/>

	</Container>
		</>
	)
}