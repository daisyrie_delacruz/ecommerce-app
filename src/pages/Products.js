import { useEffect, useState, useContext } from "react";

import {Navigate} from "react-router-dom";

import ProductCard from "../components/ProductCard";

import UserContext from "../UserContext";

import { Container } from "react-bootstrap";


export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
				);
			}))
		})
	}, []);

	return(

		(user.isAdmin)
		?
			<Navigate to ="/admin" />
		:
		<>
			<h3 className="text-center mt-5 mb-3 py-3 header shopping-header">Happy shopping!</h3>
			<Container fluid className = "prod-list pb-5" >
			{products}
			</Container>
		</>
	)
}